Source: libparse-debcontrol-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libio-stringy-perl,
                     libwww-perl,
                     libtie-ixhash-perl,
                     libtest-pod-perl,
                     libpod-coverage-perl,
                     libtest-exception-perl,
                     liberror-perl,
                     libexporter-lite-perl
Standards-Version: 3.9.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libparse-debcontrol-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libparse-debcontrol-perl.git
Homepage: https://metacpan.org/release/Parse-DebControl

Package: libparse-debcontrol-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libio-stringy-perl,
         libwww-perl,
         liberror-perl,
         libexporter-lite-perl
Recommends: libtie-ixhash-perl
Description: parser for debian control-like files
 Parse::DebControl is an easy OO way to parse Debian control files and
 other colon separated key-value pairs. It's specifically designed
 to handle the format used in Debian control files, template files, and
 the cache files used by dpkg.
 .
 For basic format information see:
 http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-controlsyntax
 .
 This module does not actually do any intelligence with the file content
 (because there are a lot of files in this format), but merely handles
 the format. It can handle simple control files, or files hundreds of lines
 long efficiently and easily.
